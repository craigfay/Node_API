/*
 * Helpers for various tasks
 * 
 */

// Dependencies
const crypto = require('crypto');
const config = require('./config');

// Containers for all the helpers
let helpers = {};

// Create a SHA256 hash
helpers.hash = (str) => {
    if (typeof str == 'string' && str.length > 0) {
        return crypto.createHmac('sha256', config.hashingSecret).update(str).digest('hex');
    } else {
        return false;
    }
};

// Parse a JSON string to an Object in all cases without throwing
helpers.parseJSONToObject = (str) => {
    try {
        const obj = JSON.parse(str);
        return obj;
    } catch(e) {
        return {};
    }
};

// Create a string of random alphanumeric characters of a given length
helpers.createRandomString = (strLength) => {
    strLength = typeof strLength == 'number' && strLength > 0 ?
        strLength : false;

    if (strLength) {
        // Define all the possible characters that could go into a string
        const possibleChars = 'abcdefghijklmnopqrstuvwxyz0123456789';

        // Start the final string
        var str = '';
        
        for (let i = 0; i < strLength; i++) {
            // Get a possible character, append it to the final string
            str += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length));
        }
        return str;

    } else {
        return false;
    }
};

 // Export the module
 module.exports = helpers;
