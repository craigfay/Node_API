/*
 * Create and export configuration variables
 *
 */

 // Container for all the environments
 let environments = {};

 // Staging (default) Environment
 environments.staging = {
    'httpPort': 8888,
    'httpsPort' : 8080,
    'envName': 'staging',
    'hashingSecret': 'secretKey'
 };

 // Production Environment
environments.production = {
   'httpPort': 8888,
   'httpsPort': 8080,
   'envName': 'production',
   'hashingSecret': 'secretKey'
};

// Determine which environment was passed as a command line argument
let currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ?
    process.env.NODE_ENV.toLowerCase() : '';

// Check that the current environment is one of the options above, otherwise default to staging
let environmentToExport = typeof(environments[currentEnvironment]) == 'object' ?
    environments[currentEnvironment] : environments.staging;

// Export the module
module.exports = environmentToExport;
