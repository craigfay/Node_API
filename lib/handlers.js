/*
 * Request Handlers
 * 
 */

// Dependencies
const _data = require('./data');
const helpers = require('./helpers');

// Define handlers
const handlers = {}

// User Handler
handlers.users = (data, callback) => {
    const acceptableMethods = ['post', 'get', 'put', 'delete'];
    if (acceptableMethods.includes(data.method)) {
        handlers._users[data.method](data, callback);
    } else {
        callback(405);
    }
};

// Container for users submethods
handlers._users = {};

// Users - Post
// Required Data: firstName, lastName, phone, password, tosAgreement
// Optional Data: none
handlers._users.post = (data, callback) => {
    // Check that all required fields are filled out
    let firstName = typeof data.payload.firstName == 'string' && data.payload.firstName.trim().length > 0 ?
        data.payload.firstName.trim() : false;

    let lastName = typeof data.payload.lastName == 'string' && data.payload.lastName.trim().length > 0 ?
        data.payload.lastName.trim() : false;
    
    let phone = typeof data.payload.phone == 'string' && data.payload.phone.trim().length == 10 ?
        data.payload.phone.trim() : false; 

    let password = typeof data.payload.password == 'string' && data.payload.password.trim().length > 0 ?
        data.payload.password.trim() : false;
    
    let tosAgreement = typeof data.payload.tosAgreement == 'boolean' && data.payload.tosAgreement == true ?
        true : false;

    if (firstName && lastName && phone && password && tosAgreement) {
        _data.read('users', phone, (err, data) => {
            if (err) {
                // User does not exist - hash the password
                const hashedPassword = helpers.hash(password);
                console.log(hashedPassword);

                if (hashedPassword) {
                    
                    // Create the user object
                    let userObject = {
                        'firstName' : firstName,
                        'lastName' : lastName,
                        'phone' : phone,
                        'hashedPassword' : hashedPassword,
                        'tosAgreement' : true
                    };

                    // Store the user
                    _data.create('users', phone, userObject, (err) => {
                        if (!err) {
                            callback(200);
                        } else {
                            console.log(err);
                            callback(500, {'Error' : 'Could not create new user'});
                        }
                    });
                } else {
                    callback(500, {'Error' : 'Could not hash password'});
                }

            } else {
                // User already exists
                callback(400, {'Error' : 'A user with that phone number already exists.'});
            }
        });
    } else {
        callback(400, {'Error' : 'Missing required fields'});
    }
};

// Users - Get
// Required data : phone
// Optional data : none
handlers._users.get = (data, callback) => {
    // Check that the phone number is valid
    const phone = (typeof data.queryStringObject.phone == 'string' && data.queryStringObject.phone.trim().length == 10) ?
        data.queryStringObject.phone.trim() : false;

    // Get the token from the headers
    const token = (typeof data.headers.token == 'string') ?
        data.headers.token : false;
    
    // Verify that the given token is valid for the given phone number
    handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
        if (tokenIsValid) {
            // Verify that a phone number was given
            if (phone) {
                // Lookup the user
                _data.read('users', phone, (err, data) => {
                    if (!err && data) {
                        // Remove the hashed password from the user object before returning it to the requester
                        delete data.hashedPassword;
                        callback(200, data);
                    } else {
                        callback(404);
                    }
                });
            } else {
                callback(400, {'Error': 'Missing required field'});
            }
        } else {
            callback(403, {'Error' : 'Missing required token in header, or token is invalid'});
        }
    });

};

// Required data: phone
// Optional data: firstName, lastName, password (at least one must be specified)
handlers._users.put = function(data,callback){
    // Check for required field
    var phone = typeof(data.payload.phone) == 'string' && data.payload.phone.trim().length == 10 ? data.payload.phone.trim() : false;
  
    // Check for optional fields
    var firstName = typeof(data.payload.firstName) == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false;
    var lastName = typeof(data.payload.lastName) == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false;
    var password = typeof(data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
  
    // Error if phone is invalid
    if(phone){
        // Error if nothing is sent to update
        if(firstName || lastName || password){

            // Get the token from the headers
            const token = (typeof data.headers.token == 'string') ?
                data.headers.token : false;

            // Verify that the given token is valid for the given phone number
            handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
                if (tokenIsValid) {

                    // Lookup the user
                    _data.read('users',phone,function(err,userData){
                        if(!err && userData){
                            // Update the fields if necessary
                            if(firstName){
                                userData.firstName = firstName;
                            }
                            if(lastName){
                                userData.lastName = lastName;
                            }
                            if(password){
                                userData.hashedPassword = helpers.hash(password);
                            }
                            // Store the new updates
                            _data.update('users',phone,userData,function(err){
                                if(!err){
                                    callback(200);
                                } else {
                                    console.log(err);
                                    callback(500,{'Error' : 'Could not update the user.'});
                                }
                            });

                        } else {
                            callback(400,{'Error' : 'Specified user does not exist.'});
                        }
                    });

                } else {
                    callback(403, {'Error' : 'Missing required token in header, or token is invalid'});
                }
            });

        } else {
            callback(400,{'Error' : 'Missing fields to update.'});
        }
    } else {
      callback(400,{'Error' : 'Missing required field.'});
    }
  };

// Users - Delete
// Required field : phone
// Optional fields : none
// @TODO Only let an authenticated user delete their object. Dont let them delete anyone else's. 
// @TODO Cleanup any other data files associated with the user
handlers._users.delete = (data, callback) => {
    // Check that the phone number is valid

    const phone = (typeof data.queryStringObject.phone == 'string' && data.queryStringObject.phone.trim().length == 10) ?
        data.queryStringObject.phone.trim() : false;

        if (phone) {
            // Lookup the user
            _data.read('users', phone, (err, data) => {
                if (!err && data) {
                    _data.delete('users', phone, (err) => {
                        if (!err) {
                            callback(200);
                        } else {
                            callback(500, {'Error' : 'Could not delete the specified user'});
                        }
                    })
                } else {
                    callback(400, {'Error' : 'Could not find the specified user'});
                }
            });
        } else {
            callback(400, {'Error': 'Missing required field'});
        }
};

// Token Handler
handlers.tokens = (data, callback) => {
    const acceptableMethods = ['post', 'get', 'put', 'delete'];
    if (acceptableMethods.includes(data.method)) {
        handlers._tokens[data.method](data, callback);
    } else {
        callback(405);
    }
};

// Container for token methods
handlers._tokens = {};

// Tokens - Get 
// Required Data : ID
// Optional Data : none
handlers._tokens.get = (data, callback) => {
    // Check that the ID is valid 
    const id = (typeof data.queryStringObject.id == 'string' && data.queryStringObject.id.trim().length == 20) ?
    data.queryStringObject.id.trim() : false;

    if (id) {
        // Lookup the user
        _data.read('tokens', id, (err, tokenData) => {
            if (!err && tokenData) {
                callback(200, tokenData);
            } else {
                callback(404);
            }
        });
    } else {
        callback(400, {'Error': 'Missing required field'});
    }
};

// Tokens - Post
// Required Data : phone, password
// Optional Data : none
handlers._tokens.post = (data, callback) => {

    let phone = typeof data.payload.phone == 'string' && data.payload.phone.trim().length == 10 ?
        data.payload.phone.trim() : false; 

    let password = typeof data.payload.password == 'string' && data.payload.password.trim().length > 0 ?
        data.payload.password.trim() : false;

    if (phone && password) {
        // Lookup the user against the phone number
        _data.read('users', phone, (err, userData) => {
            if (!err && userData) {

                // Hash the sent password, and compare it to the password stored in the user object
                const hashedPassword = helpers.hash(password); 
                if (hashedPassword == userData.hashedPassword) {

                    // If valid, create a new token with a random name. Set expiration 1 hour in the future
                    const tokenID = helpers.createRandomString(20);
                    const expires = Date.now() + 1000 * 60 * 60;
                    const tokenObject = {
                        'phone' : phone,
                        'id' : tokenID,
                        'expires' : expires
                    };

                    // Store the token
                    _data.create('tokens', tokenID, tokenObject, (err) => {
                        if (!err) {
                            callback(200, tokenObject);
                        } else {
                            callback(500, {'Error' : 'Could not create the token'});
                        }
                    });

                } else {
                    callback(400, {'Error' : 'Password is incorrect'});
                }
            } else {
                callback(400, {'Error' : 'Could not find the specified user'});
            }
        })
    } else {
        callback(400, {'Error' : 'Missing Required Fields'});
    }
}

// Tokens - Put
// Required Data : id, extend
// Optional Data : none
handlers._tokens.put = (data, callback) => {

    let id = typeof data.payload.id == 'string' && data.payload.id.trim().length == 20 ?
        data.payload.id.trim() : false; 

    let extend = typeof data.payload.extend == 'boolean' && data.payload.extend == true ?
        data.payload.id.trim() : false; 

    if (id && extend) {
        // Lookup the token
        _data.read('tokens', id, (err, tokenData) => {
            if (!err && tokenData) {
                // Check to make sure the token isn't already expired
                if (tokenData.expires > Date.now()) {
                    // Set the expiration an hour from now
                    tokenData.expires = Date.now() + 1000 * 60 * 60;

                    // Store the new updates
                    _data.update('tokens', id, tokenData, (err) => {
                        if (!err) {
                            callback(200);
                        } else {
                            callback(500, {'Error' : 'Could not update the token\'s expiration'});
                        }
                    });
                } else {
                    callback(400, {'Error' : 'The token has already expired, and cannot be extended'});
                }
            } else {
                callback(400, {'Error' : 'Specified token does not exist'});
            }
        });
    } else {
        callback(400, {'Error' : 'Missing required field(s) or field(s) are invalid'});
    }
}

// Tokens - Delete 
// Required Data : ID
// Optional Data : none
handlers._tokens.delete = (data, callback) => {
    // Check for a valid ID
    const id = (typeof data.queryStringObject.id == 'string' && data.queryStringObject.id.trim().length == 20) ?
        data.queryStringObject.id.trim() : false;

        if (id) {
            // Lookup the token 
            _data.read('tokens', id, (err, data) => {
                if (!err && data) {
                    _data.delete('tokens', id, (err) => {
                        if (!err) {
                            callback(200);
                        } else {
                            callback(500, {'Error' : 'Could not delete the specified token'});
                        }
                    })
                } else {
                    callback(400, {'Error' : 'Could not find the specified token'});
                }
            });
        } else {
            callback(400, {'Error': 'Missing required field'});
        }
};

// Verify that a given token id is currently valid for a given user
handlers._tokens.verifyToken = (id, phone, callback) => {
    // Lookup the token
    _data.read('tokens', id, (err, tokenData) => {
        if (!err && tokenData) {
            // Check that the token is for the given user and not expired
            if (tokenData.phone == phone && tokenData.expires > Date.now()) {
                callback(true);
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    });
};

// Ping Handler
handlers.ping = (data, callback) => {
    callback(200);
};

// Not Found Handler
handlers.notFound = (data, callback) => {
    callback(404);
};

// Export the module
module.exports = handlers;
